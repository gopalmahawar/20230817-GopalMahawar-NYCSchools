## NYC School App

NYCSchoolApp demonstrates the usage of NYC schools API to show list of schools and a dialog displaying the details of the school.

## Tech stack & Open-source libraries

NYCSchoolApp is a native Android app build using MVVM architecture.
- Minimum SDK level 24
- Kotlin based
- Coroutines for asynchronously execution
- Hilt for dependency injection.
- JetPack
    - ViewModel - UI related data holder, lifecycle aware.
    - LiveData - Observe state changes
- Architecture
    - MVVM Architecture (Model - View - ViewModel)
- Retrofit2 for REST APIs.
- Mockito - mocking framework for unit testing.

### App Screenshot

<img width="393" alt="School List Screen" src="/snapshots/screenshot_school_list.png">

<img width="395" alt="School Detail Screen" src="/snapshots/screenshot_school_details.png">
