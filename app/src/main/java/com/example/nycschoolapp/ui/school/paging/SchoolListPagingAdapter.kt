package com.example.nycschoolapp.ui.school.paging

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.nycschoolapp.databinding.SchoolListItemBinding
import com.example.nycschoolapp.models.SchoolDetails
import com.example.nycschoolapp.ui.details.SchoolDetailsFragment
import com.example.nycschoolapp.ui.school.adapter.SchoolPagedViewHolder

class SchoolListPagingAdapter(private val context: Context) :
    PagingDataAdapter<SchoolDetails, SchoolPagedViewHolder>(COMPARATOR) {

    private var inflater: LayoutInflater? = null

    override fun onBindViewHolder(holder: SchoolPagedViewHolder, position: Int) {

        val schoolDetails = getItem(position)
        holder.bind(schoolDetails)
        holder.itemBinding.schoolRowCL.setOnClickListener { v: View? ->
            val fragmentManager = (context as AppCompatActivity).supportFragmentManager
            val schoolDetailsFragment: SchoolDetailsFragment =
                SchoolDetailsFragment.newInstance()
            val args = Bundle()
            args.putParcelable(SCHOOL_DETAILS, getItem(position))
            schoolDetailsFragment.arguments = args
            schoolDetailsFragment.show(fragmentManager, "school_details")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolPagedViewHolder {

        if (inflater == null) {
            inflater = LayoutInflater.from(parent.context)
        }
        val itemBinding = SchoolListItemBinding.inflate(
            inflater!!, parent, false
        )
        return SchoolPagedViewHolder(itemBinding)
    }

    companion object {
        const val SCHOOL_DETAILS = "SchoolDetails"
        private val COMPARATOR = object : DiffUtil.ItemCallback<SchoolDetails>() {
            override fun areItemsTheSame(oldItem: SchoolDetails, newItem: SchoolDetails): Boolean {
                return oldItem.dbn == newItem.dbn
            }

            override fun areContentsTheSame(
                oldItem: SchoolDetails,
                newItem: SchoolDetails
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}