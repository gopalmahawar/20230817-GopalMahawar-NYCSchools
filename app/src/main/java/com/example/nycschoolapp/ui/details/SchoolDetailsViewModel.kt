package com.example.nycschoolapp.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.nycschoolapp.repositories.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {

    fun getSatDetails(dbn: String) = liveData(Dispatchers.IO) {
        emit(appRepository.getSatScore(dbn))
    }
}