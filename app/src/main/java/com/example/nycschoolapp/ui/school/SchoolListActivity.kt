package com.example.nycschoolapp.ui.school

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschoolapp.ui.school.paging.SchoolListPagingAdapter
import com.example.nycschoolapp.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolListActivity : AppCompatActivity() {

    private lateinit var mViewModel: SchoolListViewModel
    private lateinit var recyclerView: RecyclerView
    lateinit var adapter: SchoolListPagingAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_list)

        recyclerView = findViewById(R.id.recyclerView)
        mViewModel = ViewModelProvider(this)[SchoolListViewModel::class.java]

        adapter = SchoolListPagingAdapter(this)

        // Setup recycler view
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = adapter

        mViewModel.list.observe(this, Observer {
            adapter.submitData(lifecycle, it)
        })
    }
}