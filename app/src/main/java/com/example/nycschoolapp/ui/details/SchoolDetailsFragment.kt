package com.example.nycschoolapp.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.nycschoolapp.R
import com.example.nycschoolapp.databinding.SchoolDetailsFragmentBinding
import com.example.nycschoolapp.models.SchoolDetails
import com.example.nycschoolapp.ui.school.paging.SchoolListPagingAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsFragment : DialogFragment() {

    private lateinit var viewModel: SchoolDetailsViewModel
    private lateinit var binding: SchoolDetailsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.school_details_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this)[SchoolDetailsViewModel::class.java]
        if (arguments != null) {
            val schoolDetails =
                requireArguments().getParcelable<SchoolDetails>(SchoolListPagingAdapter.SCHOOL_DETAILS)
            binding.schoolDetails = schoolDetails
            viewModel.getSatDetails(schoolDetails?.dbn!!)
                .observe(this) { satScores ->
                    if (satScores.isNullOrEmpty()) {
                        binding.satBodyCL.visibility = View.GONE
                        binding.satNoResultBodyCL.visibility = View.VISIBLE
                    } else {
                        binding.satScores = satScores[0]
                        binding.satBodyCL.visibility = View.VISIBLE
                        binding.satNoResultBodyCL.visibility = View.GONE
                    }
                }
            binding.closeButton.setOnClickListener { dismiss() }
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window!!.setLayout(width, height)
        }
    }

    companion object {
        fun newInstance(): SchoolDetailsFragment {
            return SchoolDetailsFragment()
        }
    }
}