package com.example.nycschoolapp.ui.school.adapter

import androidx.recyclerview.widget.RecyclerView
import com.example.nycschoolapp.databinding.SchoolListItemBinding
import com.example.nycschoolapp.models.SchoolDetails

class SchoolPagedViewHolder internal constructor(val itemBinding: SchoolListItemBinding) :
    RecyclerView.ViewHolder(
        itemBinding.root
    ) {

    fun bind(schoolDetails: SchoolDetails?) {
        itemBinding.schoolDetails = schoolDetails
    }
}