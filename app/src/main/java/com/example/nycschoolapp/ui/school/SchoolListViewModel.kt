package com.example.nycschoolapp.ui.school

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.nycschoolapp.repositories.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(private val appRepository: AppRepository) :
    ViewModel() {

    val list = appRepository.getSchoolList().cachedIn(viewModelScope)
}