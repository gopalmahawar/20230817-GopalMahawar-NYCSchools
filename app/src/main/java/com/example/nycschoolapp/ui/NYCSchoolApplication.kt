package com.example.nycschoolapp.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class NYCSchoolApplication : Application()