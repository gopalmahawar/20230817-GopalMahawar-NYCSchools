package com.example.nycschoolapp.ui.school.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.nycschoolapp.models.SchoolDetails
import com.example.nycschoolapp.services.WebService
import java.lang.Exception

class SchoolListPagingSource(private val webService: WebService) :
    PagingSource<Int, SchoolDetails>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, SchoolDetails> {
        return try {
            val position = params.key ?: PAGE_START_INDEX
            val response = webService.getSchoolDetails(
                PAGE_SIZE,
                position
            )
            return LoadResult.Page(
                data = response,
                prevKey = if (position == PAGE_START_INDEX) null else position - PAGE_SIZE,
                nextKey = position + PAGE_SIZE
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, SchoolDetails>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(PAGE_SIZE) ?: anchorPage?.nextKey?.minus(PAGE_SIZE)
        }
    }

    companion object {
        private const val PAGE_START_INDEX = 0
        const val PAGE_SIZE = 20
    }
}