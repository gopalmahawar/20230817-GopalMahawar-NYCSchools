package com.example.nycschoolapp.services

import com.example.nycschoolapp.models.SatScores
import com.example.nycschoolapp.models.SchoolDetails
import retrofit2.http.GET
import retrofit2.http.Query

interface WebService {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolDetails(
        @Query("\$limit") pageSize: Int,
        @Query("\$offset") startIndex: Int
    ): List<SchoolDetails>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSatScore(@Query("dbn") dbn: String): List<SatScores>
}