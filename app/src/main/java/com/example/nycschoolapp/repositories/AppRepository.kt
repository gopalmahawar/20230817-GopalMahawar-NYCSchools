package com.example.nycschoolapp.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.example.nycschoolapp.ui.school.paging.SchoolListPagingSource
import com.example.nycschoolapp.services.WebService
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val webService: WebService
) {
    fun getSchoolList() = Pager(
        config = PagingConfig(pageSize = 20, maxSize = 100),
        pagingSourceFactory = { SchoolListPagingSource(webService) }
    ).liveData

    suspend fun getSatScore(dbn: String) = webService.getSatScore(dbn)
}