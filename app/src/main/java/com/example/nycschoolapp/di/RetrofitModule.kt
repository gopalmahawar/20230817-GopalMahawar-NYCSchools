package com.example.nycschoolapp.di

import com.example.nycschoolapp.services.WebService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RetrofitModule {

    @Singleton
    @Provides
    fun getRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

    @Singleton
    @Provides
    fun getWebServiceAPI(retrofit: Retrofit): WebService {
        return retrofit.create(WebService::class.java)
    }

    companion object {
        private const val BASE_URL = "https://data.cityofnewyork.us/"
    }
}