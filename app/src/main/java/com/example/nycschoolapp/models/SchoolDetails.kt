package com.example.nycschoolapp.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class SchoolDetails(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("school_name") val schoolName: String?,
    @SerializedName("total_students") val totalStudents: String?,
    @SerializedName("primary_address_line_1") val primaryAddressLine1: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("state_code") val stateCode: String?,
    @SerializedName("zip") val zip: String?
) : Parcelable