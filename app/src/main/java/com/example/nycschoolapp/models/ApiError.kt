package com.example.nycschoolapp.models

data class ApiError(
    val statusCode: Int = 0,
    val message: String = "Unknown Error."
)
