package com.example.nycschoolapp.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschoolapp.models.SatScores
import com.example.nycschoolapp.repositories.AppRepository
import com.example.nycschoolapp.ui.details.SchoolDetailsViewModel
import com.example.nycschoolapp.utils.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

@ExperimentalCoroutinesApi
class SchoolDetailsViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = UnconfinedTestDispatcher()
    private val repository = mock(AppRepository::class.java)
    private lateinit var viewModel: SchoolDetailsViewModel

    @Before
    fun setup() {
        viewModel = SchoolDetailsViewModel(repository)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun fetchSatScores_isSuccess() = runBlocking {

        val satScore = SatScores(
            dbn = "111",
            numOfSatTestTakers = "29",
            satMathAvgScore = "404",
            satCriticalReadingAvgScore = "363",
            satWritingAvgScore = "355"
        )

        `when`(repository.getSatScore("111")).thenReturn((listOf(satScore)))
        val res = viewModel.getSatDetails("111").getOrAwaitValue()
        assertEquals(listOf(satScore), res)
        assertEquals(1, res.size)
    }

    @Test
    fun fetchSatScores_isFailure() = runBlocking {

        val satScore = SatScores(
            dbn = "111",
            numOfSatTestTakers = "29",
            satMathAvgScore = "404",
            satCriticalReadingAvgScore = "363",
            satWritingAvgScore = "355"
        )

        `when`(repository.getSatScore("111")).thenReturn((listOf(satScore)))
        val result = viewModel.getSatDetails("11").getOrAwaitValue()
        assertNull(result)
    }
}