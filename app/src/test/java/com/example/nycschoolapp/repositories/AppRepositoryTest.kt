package com.example.nycschoolapp.repositories

import com.example.nycschoolapp.models.SatScores
import com.example.nycschoolapp.services.WebService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.Mockito.`when`

@ExperimentalCoroutinesApi
class AppRepositoryTest {

    private val webService = mock<WebService>()
    private val appRepository = AppRepository(webService)

    @Test
    fun getSatScores_isSuccess() = runBlocking {

        val satScore = SatScores(
            dbn = "N123",
            numOfSatTestTakers = "29",
            satMathAvgScore = "404",
            satCriticalReadingAvgScore = "363",
            satWritingAvgScore = "355"
        )

        `when`(webService.getSatScore("N123")).thenReturn(listOf(satScore))
        val result = appRepository.getSatScore("N123")
        assertEquals(satScore, result[0])
        assertEquals("404", result[0].satMathAvgScore)
    }

    @Test
    fun getSatScores_isFailure() = runBlocking {

        val satScore = SatScores(
            dbn = "N123",
            numOfSatTestTakers = "29",
            satMathAvgScore = "404",
            satCriticalReadingAvgScore = "363",
            satWritingAvgScore = "355"
        )

        `when`(webService.getSatScore("N123")).thenReturn(listOf(satScore))
        val result = appRepository.getSatScore("123")
        assertNull(result)
    }
}